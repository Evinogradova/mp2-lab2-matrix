##Верхнетреугольные матрицы на шаблонах

####Цели и задачи

*Цель данной работы* -- создание программных средств, поддерживающих эффективное хранение матриц специального вида (верхнетреугольных) и выполнение основных операций над ними:
- сложение/вычитание;
- копирование;
- сравнение.

*Выполнение работы предполагает решение следующих задач:*

1. Реализация методов шаблонного класса `TVector` согласно заданному интерфейсу.
2. Реализация методов шаблонного класса `TMatrix` согласно заданному интерфейсу.
3. Обеспечение работоспособности тестов и примера использования.
4. Реализация заготовок тестов, покрывающих все методы классов `TVector` и `TMatrix`.
5. Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.

Перед выполнением работы был получен проект-шаблон, содержащий:

- Интерфейсы классов Вектор и Матрица (h-файл)
- Начальный набор готовых тестов для каждого из указанных классов.
- Набор заготовок тестов для каждого из указанных классов.
- Тестовый пример использования класса Матрица

####Особенности реализации

Хранить верхнетреугольную матрицу можно несколькими способами:
1. Как обычную матрицу, без исключения нулевых элементов (при этом половина памяти не будет использоваться)
2. Хранить строки матрицы линейно друг за другом (что достаточно затруднительно при больших размерах матрицы)
3. Представление матрицы в виде набора векторов
4. Матрица как вектор векторных элементов (именно этот способ реализован в данной работе)

Удобно сначала реализовать класс векторов, а потом, на его основе, -- класс матриц.

####Реализация методов шаблонных классов TVector и TMatrix

```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// utmatrix.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Верхнетреугольная матрица - реализация на основе шаблона вектора

#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;

// Шаблон вектора
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // размер вектора
  int StartIndex; // индекс первого элемента вектора
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // конструктор копирования
  ~TVector();
  int GetSize()      { return Size;       } // размер вектора
  int GetStartIndex(){ return StartIndex; } // индекс первого элемента
  ValType& operator[](int pos);             // доступ
  bool operator==(const TVector &v) const;  // сравнение
  bool operator!=(const TVector &v) const;  // сравнение
  TVector& operator=(const TVector &v);     // присваивание

  // скалярные операции
  TVector  operator+(const ValType &val);   // прибавить скаляр
  TVector  operator-(const ValType &val);   // вычесть скаляр
  TVector  operator*(const ValType &val);   // умножить на скаляр

  // векторные операции
  TVector  operator+(const TVector &v);     // сложение
  TVector  operator-(const TVector &v);     // вычитание
  ValType  operator*(const TVector &v);     // скалярное произведение

  // ввод-вывод
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};

template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s < 0 || s >= MAX_VECTOR_SIZE)
		throw "Incorrect size";
	if (si < 0)
		throw "Negative start index";
	Size = s;
	StartIndex = si;
	pVector = new ValType[Size];
} /*-------------------------------------------------------------------------*/

template <class ValType> //конструктор копирования
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = new ValType[Size];
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} /*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} /*-------------------------------------------------------------------------*/

template <class ValType> // доступ
ValType& TVector<ValType>::operator[](int pos)
{
	if (pos < StartIndex || pos >= Size + StartIndex)
		throw "Incorrect index";
	return pVector[pos - StartIndex];
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator==(const TVector &v) const
{
	if (Size != v.Size)
		return 0;
	for (int i = 0; i < Size; i++)
	if (pVector[i] != v.pVector[i])
		return 0;
	return 1;
	
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !(*this == v);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (*this != v)
	{
		delete[] pVector;
		Size = v.Size;
		StartIndex = v.StartIndex;
		pVector = new ValType[Size];
		for (int i = 0; i < Size; i++)
			pVector[i] = v.pVector[i];
	}
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // прибавить скаляр
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] += val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычесть скаляр
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] -= val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // умножить на скаляр
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] *= val;
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Incorrect size";
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] = pVector[i] + v.pVector[i];
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Incorrect size";
	TVector<ValType> tmp(*this);
	for (int i = 0; i < Size; i++)
		tmp.pVector[i] = pVector[i] - v.pVector[i];
	return tmp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // скалярное произведение
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if (Size != v.Size)
		throw "Incorrect size";
	ValType scal = 0;
	for (int i = 0; i < Size; i++)
		scal += pVector[i] * v.pVector[i];
	return scal;
} /*-------------------------------------------------------------------------*/


// Верхнетреугольная матрица
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // копирование
  TMatrix(const TVector<TVector<ValType> > &mt); // преобразование типа
  bool operator==(const TMatrix &mt) const;      // сравнение
  bool operator!=(const TMatrix &mt) const;      // сравнение
  TMatrix& operator= (const TMatrix &mt);        // присваивание
  TMatrix  operator+ (const TMatrix &mt);        // сложение
  TMatrix  operator- (const TMatrix &mt);        // вычитание

  // ввод / вывод
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};

template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s < 0 || s >= MAX_MATRIX_SIZE)
		throw "Incorrect size";
	for (int i = 0; i < s; i++)
		pVector[i] = TVector<ValType>(s - i, i);
} /*-------------------------------------------------------------------------*/

template <class ValType> // конструктор копирования
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // конструктор преобразования типа
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // сравнение
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator==(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	TVector<TVector<ValType> >::operator=(mt);
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator-(mt);
} /*-------------------------------------------------------------------------*/

// TVector О3 Л2 П4 С6
// TMatrix О2 Л2 П3 С3
#endif
```

####Реализация тестов для класса TVector

```cpp
#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> v1(3);
	// v1 = 3 9 -4
	v1[0] = 3;
	v1[1] = 9;
	v1[2] = -4;

	TVector<int> v2(v1);

	EXPECT_EQ(v1, v2);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> v1;
	TVector<int> v2(v1);

	EXPECT_NE(&v1, &v2);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[-1] = 5);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[6] = 3);
}

TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 3;
	TVector<int> v(size), expV(size);
	// v = 5 2 8
	v[0] = 5;
	v[1] = 2;
	v[2] = 8;
	// expV = 5 2 8
	expV[0] = 5;
	expV[1] = 2;
	expV[2] = 8;

	EXPECT_EQ(expV, v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size);
	// v1 = -3 2 6
	v1[0] = -3;
	v1[1] = 2;
	v1[2] = 6;

	EXPECT_EQ(v1, v2 = v1);
}

TEST(TVector, assign_operator_change_vector_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);
	for (int i = 0; i < size1; i++)
		v1[i] = 1;
	v2 = v1;

	EXPECT_EQ(size1, v2.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);
	for (int i = 0; i < size1; i++)
		v1[i] = 1;

	EXPECT_EQ(v1, v2 = v1);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	const int size = 3;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
	}

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v(3);
	for (int i = 0; i < 3; i++)
		v[i] = 1;

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	EXPECT_FALSE(v1 == v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 3 -9 4
	v[0] = 3;
	v[1] = -9;
	v[2] = 4;
	// expV = 5 -7 6
	expV[0] = 5;
	expV[1] = -7;
	expV[2] = 6;

	EXPECT_EQ(expV, v + scal);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 5 -7 6
	v[0] = 5;
	v[1] = -7;
	v[2] = 6;
	// expV = 3 -9 4
	expV[0] = 3;
	expV[1] = -9;
	expV[2] = 4;

	EXPECT_EQ(expV, v - scal);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 3 -9 4
	v[0] = 3;
	v[1] = -9;
	v[2] = 4;
	// expV = 6 -18 8
	expV[0] = 6;
	expV[1] = -18;
	expV[2] = 8;

	EXPECT_EQ(expV, v * scal);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size), expV(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
		expV[i] = 2;
	}

	EXPECT_EQ(expV, v1 + v2);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size), expV(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 2;
		v2[i] = 1;
		expV[i] = 1;
	}

	EXPECT_EQ(expV, v1 - v2);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 3, scal = 3;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
	}

	EXPECT_EQ(scal, v1 * v2);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 * v2);
}
```

####Реализация тестов для класса TMatrix

```cpp
#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	const int size = 3;
	TMatrix<int> m1(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 1;
	TMatrix<int> m2(m1);

	EXPECT_EQ(m1, m2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	const int size = 3;
	TMatrix<int> m1(size);
	TMatrix<int> m2(m1);

	EXPECT_NE(&m1, &m2);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<int> m(3);

	EXPECT_EQ(3, m.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> m(3);
	m[0][2] = 13;

	EXPECT_EQ(13, m[0][2]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> m(3);

	ASSERT_ANY_THROW(m[-1][0] = 2);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> m(3);

	ASSERT_ANY_THROW(m[4][5] = 2);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	const int size = 3;
	TMatrix<int> m(size), expM(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m[i][j] = 1;
			expM[i][j] = 1;
		}

	EXPECT_EQ(expM, m = m);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	const int size = 3;
	TMatrix<int> m1(size), m2(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
			m1[i][j] = 1;

	EXPECT_EQ(m1, m2 = m1);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	const int size1 = 2, size2 = 5;
	TMatrix<int> m1(size1), m2(size2);
	for (int i = 0; i < size1; i++)
		for (int j = i; j < size1; j++)
			m1[i][j] = 1;
	m2 = m1;

	EXPECT_EQ(size1, m2.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	const int size1 = 2, size2 = 5;
	TMatrix<int> m1(size1), m2(size2);
	for (int i = 0; i < size1; i++)
		for (int j = i; j < size1; j++)
			m1[i][j] = 1;

	EXPECT_EQ(m1, m2 = m1);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	const int size = 3;
	TMatrix<int> m1(size), m2(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 1;
			m2[i][j] = 1;
		}

	EXPECT_TRUE(m1 == m2);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> m(3);
	for (int i = 0; i < 3; i++)
		for (int j = i; j < 3; j++)
			m[i][j] = 1;

	EXPECT_TRUE(m == m);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	const int size1 = 2, size2 = 5;
	TMatrix<int> m1(size1), m2(size2);

	EXPECT_FALSE(m1 == m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	const int size = 3;
	TMatrix<int> m1(size), m2(size), expM(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 1;
			m2[i][j] = 1;
			expM[i][j] = 2;
		}

	EXPECT_EQ(expM, m1 + m2);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TMatrix<int> m1(size1), m2(size2);

	ASSERT_ANY_THROW(m1 + m2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	const int size = 3;
	TMatrix<int> m1(size), m2(size), expM(size);
	for (int i = 0; i < size; i++)
		for (int j = i; j < size; j++)
		{
			m1[i][j] = 2;
			m2[i][j] = 1;
			expM[i][j] = 1;
		}

	EXPECT_EQ(expM, m1 - m2);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TMatrix<int> m1(size1), m2(size2);

	ASSERT_ANY_THROW(m1 - m2);
}
```

####Прохождение тестов

![Tests.png](http://i062.radikal.ru/1611/a8/93c37d70e06a.png)

####Реализация тестового приложения

```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// sample_matrix.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (20.04.2015)
//
// Тестирование верхнетреугольной матрицы

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	try
	{
		int i, j, size;

		setlocale(LC_ALL, "Russian");
		cout << "Тестирование программ поддержки представления треугольных матриц"
			<< endl << "Введите размер матриц:" << endl;
		cin >> size;
		TMatrix<int> a(size), b(size), c(size), d;
		cout << "Введите матрицу a" << endl;
		cin >> a;
		for (i = 0; i < size; i++)
			for (j = i; j < size; j++)
			{
				b[i][j] = i * 10 + j;
			}
		cout << "Matrix a = " << endl << a << endl;
		cout << "Matrix b = " << endl << b << endl;
		c = a + b;
		cout << "Matrix c = a + b" << endl << c << endl;
		c = a - b;
		cout << "Matrix c = a - b" << endl << c << endl;
		d = a;
		cout << "Matrix d = a" << endl << d << endl;
	}
	catch (string *err)
	{
		cout << err << endl;
	}
}
//---------------------------------------------------------------------------
```

####Результат работы тестового приложения

![SampleMatrix.png](http://s017.radikal.ru/i419/1611/60/a39015bf5f50.png)

####Вывод

В ходе выполнения данной работы были реализованы шаблонные классы `TVector` и `TMatrix`, причем класс `TMatrix` наследует класс `TVector`.

Также были реализованы тесты для этих классов на основе системы тестирования Google Test.

Оба класса реализованы как шаблоны, что дает возможность использовать произвольный тип данных в матрице и, в совокупности с наследованием, позволяет значительно упростить реализацию класса `TMatrix`.