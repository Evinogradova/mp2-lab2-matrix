#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> v1(3);
	// v1 = 3 9 -4
	v1[0] = 3;
	v1[1] = 9;
	v1[2] = -4;

	TVector<int> v2(v1);

	EXPECT_EQ(v1, v2);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> v1;
	TVector<int> v2(v1);

	EXPECT_NE(&v1, &v2);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[-1] = 5);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);

	ASSERT_ANY_THROW(v[6] = 3);
}

TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 3;
	TVector<int> v(size), expV(size);
	// v = 5 2 8
	v[0] = 5;
	v[1] = 2;
	v[2] = 8;
	// expV = 5 2 8
	expV[0] = 5;
	expV[1] = 2;
	expV[2] = 8;

	EXPECT_EQ(expV, v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size);
	// v1 = -3 2 6
	v1[0] = -3;
	v1[1] = 2;
	v1[2] = 6;

	EXPECT_EQ(v1, v2 = v1);
}

TEST(TVector, assign_operator_change_vector_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);
	for (int i = 0; i < size1; i++)
		v1[i] = 1;
	v2 = v1;

	EXPECT_EQ(size1, v2.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);
	for (int i = 0; i < size1; i++)
		v1[i] = 1;

	EXPECT_EQ(v1, v2 = v1);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	const int size = 3;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
	}

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v(3);
	for (int i = 0; i < 3; i++)
		v[i] = 1;

	EXPECT_TRUE(v == v);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	EXPECT_FALSE(v1 == v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 3 -9 4
	v[0] = 3;
	v[1] = -9;
	v[2] = 4;
	// expV = 5 -7 6
	expV[0] = 5;
	expV[1] = -7;
	expV[2] = 6;

	EXPECT_EQ(expV, v + scal);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 5 -7 6
	v[0] = 5;
	v[1] = -7;
	v[2] = 6;
	// expV = 3 -9 4
	expV[0] = 3;
	expV[1] = -9;
	expV[2] = 4;

	EXPECT_EQ(expV, v - scal);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	const int size = 3, scal = 2;
	TVector<int> v(size), expV(size);
	// v = 3 -9 4
	v[0] = 3;
	v[1] = -9;
	v[2] = 4;
	// expV = 6 -18 8
	expV[0] = 6;
	expV[1] = -18;
	expV[2] = 8;

	EXPECT_EQ(expV, v * scal);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size), expV(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
		expV[i] = 2;
	}

	EXPECT_EQ(expV, v1 + v2);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 3;
	TVector<int> v1(size), v2(size), expV(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 2;
		v2[i] = 1;
		expV[i] = 1;
	}

	EXPECT_EQ(expV, v1 - v2);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 3, scal = 3;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
	{
		v1[i] = 1;
		v2[i] = 1;
	}

	EXPECT_EQ(scal, v1 * v2);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	const int size1 = 2, size2 = 5;
	TVector<int> v1(size1), v2(size2);

	ASSERT_ANY_THROW(v1 * v2);
}

