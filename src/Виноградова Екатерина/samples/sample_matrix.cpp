// ����, ���, ���� "������ ����������������-2", �++, ���
//
// sample_matrix.cpp - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (20.04.2015)
//
// ������������ ����������������� �������

#include <iostream>
#include "utmatrix.h"
//---------------------------------------------------------------------------

void main()
{
	try
	{
		int i, j, size;

		setlocale(LC_ALL, "Russian");
		cout << "������������ �������� ��������� ������������� ����������� ������"
			<< endl << "������� ������ ������:" << endl;
		cin >> size;
		TMatrix<int> a(size), b(size), c(size), d;
		cout << "������� ������� a" << endl;
		cin >> a;
		for (i = 0; i < size; i++)
			for (j = i; j < size; j++)
			{
				b[i][j] = i * 10 + j;
			}
		cout << "Matrix a = " << endl << a << endl;
		cout << "Matrix b = " << endl << b << endl;
		c = a + b;
		cout << "Matrix c = a + b" << endl << c << endl;
		c = a - b;
		cout << "Matrix c = a - b" << endl << c << endl;
		d = a;
		cout << "Matrix d = a" << endl << d << endl;
	}
	catch (string *err)
	{
		cout << err << endl;
	}
}
//---------------------------------------------------------------------------
